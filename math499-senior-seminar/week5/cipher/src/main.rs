fn encrypt(n: i32, k: i32) -> i32 {
    (n + k) % 256
}

fn decrypt(n: &u8, k: i32) -> i32 {
    (i32::from(*n) - k) % 256
}

fn ch_to_int(c: &u8) -> i32 {
    i32::try_from(*c).unwrap()
}

fn int_to_ch(i: i32) -> u8 {
    u8::try_from(i).unwrap()
}

fn main() {
    let message = String::from("ET TU, BRUTE?");
    println!("Message: {}", message);
    let k = 7;
    let encrypted: Vec<u8> = message.as_bytes().iter().map(ch_to_int).map(|n| encrypt(n, k)).map(int_to_ch).collect();
    println!("Encrypted: {}", String::from_utf8_lossy(encrypted.as_slice()));

    let decrypted: Vec<u8> = encrypted.iter().map(|n| decrypt(n, k)).map(int_to_ch).collect();
    println!("Decrypted: {}", String::from_utf8_lossy(decrypted.as_slice()));
    // Encrypted: L['[\3'IY\[LF
    // Decrypted: ET TU, BRUTE?

}
