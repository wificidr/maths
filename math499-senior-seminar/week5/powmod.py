import random
random.seed(0)

a_s = [random.randint(37, 65537) for _ in range(100)]
k_s = [random.randint(37, 65537) for _ in range(100)]

def powmod(a: int, k: int, m: int) -> int:
    a_i = a
    a_k = a if k & 1 else 1
    for i in range(1, k.bit_length()):
        a_i = a_i**2 % m
        if (1 << i) & k:
            a_k *= a_i
    return a_k % m

def powmodv2(a: int, k: int, m: int) -> int:
    a_i = a
    a_k = a if k & 1 else 1
    for i in range(1, 9):
        a_i = a_i**2 % m
        b = ((1 << i) & k) >> i
        a_k *= (a_i * b + b ^ 1)
    return a_k % m


def testme():
    for a in a_s:
        for k in k_s:
            powmod(a, k, 307)

def testmev2():
    for a in a_s:
        for k in k_s:
            powmodv2(a, k, 307)

def testpy():
    for a in a_s:
        for k in k_s:
            pow(a, k, 307)

print(powmod(26, 307, 47))
print(powmodv2(26, 307, 47))