\documentclass[a4paper,12pt,final]{article}
\usepackage[utf8]{inputenc} % Required for inputting international characters
\usepackage[T1]{fontenc} % Output font encoding for international characters
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{pifont}
\usepackage{tikz}
\usepackage{color}
\usepackage{microtype}
\usepackage{wrapfig}
\usepackage{listings}
\usepackage{pythonhighlight}
\usepackage{hyperref}
\usepackage{tabularx}
\usepackage{tabularray}
\usepackage[shortlabels]{enumitem}
\usetikzlibrary {positioning}

\usepackage{mathpazo} % Palatino font

\usepackage[backend=biber,
style=apa,
citestyle=authoryear
]{biblatex}

\lstset{ % General setup for the package
    language=C,
    basicstyle=\scriptsize\sffamily,
    frame=tb,
    tabsize=2,
    columns=fixed,
    showstringspaces=false,
    showtabs=false,
    keepspaces,
    commentstyle=\color{red},
    keywordstyle=\color{blue}
}
\lstset{
morekeywords={fn,u8,Vec,let,in},
}

\DeclareCiteCommand{\citeauthorfirstlast}
  {\boolfalse{citetracker}%
   \boolfalse{pagetracker}%
   \DeclareNameAlias{labelname}{given-family}%
   \usebibmacro{prenote}}
  {\ifciteindex
     {\indexnames{labelname}}
     {}%
   \printnames{labelname}}
  {\multicitedelim}
  {\usebibmacro{postnote}}

\addbibresource{references.bib}
\begin{document}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\begin{titlepage} % Suppresses displaying the page number on the title page and the subsequent page counts as page 1
	\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for horizontal lines, change thickness here

	\center % Centre everything on the page

	%------------------------------------------------
	%	Headings
	%------------------------------------------------

	\textsc{\LARGE American Public University}\\[1.5cm] % Main heading such as the name of your university/college

	%------------------------------------------------
	%	Title
	%------------------------------------------------

	\HRule\\[0.4cm]

	{\huge\bfseries The RSA Algorithm MATH499}\\[0.4cm] % Title of your document

	\HRule\\[1.5cm]

	%------------------------------------------------
	%	Author(s)
	%------------------------------------------------

	\begin{minipage}{0.4\textwidth}
		\begin{flushleft}
			\large
			\textit{Author}\\
			Daniel \textsc{Justice} % Your name
		\end{flushleft}
	\end{minipage}
	~
	\begin{minipage}{0.4\textwidth}
		\begin{flushright}
			\large
			\textit{Professor}\\
			Dr. Stephen \textsc{Flink} % Supervisor's name
		\end{flushright}
	\end{minipage}

	%------------------------------------------------
	%	Date
	%------------------------------------------------

	\vfill\vfill\vfill
	{\large\today} % Date, change the \today to a set date if you want to be precise
	\vfill

\end{titlepage}

%----------------------------------------------------------------------------------------

\clearpage

\Large
 \begin{center}
The RSA Algorithm \\

\hspace{10pt}

\large
Daniel Justice$^1$

\hspace{10pt}

\small
$^1$) American Public University\\
daniel.justice@mycampus.apus.edu\\

\end{center}

\hspace{10pt}

\normalsize

This is an attempt at explaining the RSA cryptographic algorithm at a level understandable by most math undergraduates.
Understanding the algorithm does not require any knowledge of computer science, but it is helpful when reading the code snippets.
The following pages will explore the fundamentals of Number Theory and provide a short introduction to Group Theory concepts.
The RSA algorithm exemplifies the application of ideas that originated in the study of pure mathematics.

\clearpage

\newtheoremstyle{foo}%
{}{}%
{\sffamily}{}%
{\bfseries}{.}%
{.5em}{}
\theoremstyle{foo}
\newtheorem*{thm}{Theorem}

\theoremstyle{definition}
\newtheorem*{dfn}{Definition}

\section{Introduction}

The algorithm developed by Rivest, Shamir, and Adleman allows two parties to exchange secrets without the need to covertly share a passphrase over some other secure channel such as a courier (\citeauthorfirstlast{history}).
Modular arithmetic and prime numbers power the techniques used to securely move sensitive data on a daily basis.
The internet would look very different today without the critical ability to exchange sensitive information over a shared and untrusted medium.
The development and growth of the internet in the 20th century created a pressing need for public key cryptography systems.
Modern ciphers are largely based on properties of the prime numbers which were originally studied by the ancient Greeks.
Cryptography has been a constant battle of cat and mouse.
As new schemes are developed, attackers find ways to break them, spurring the development of new techniques.
The current state of the art uses the RSA Algorithm to protect sensitive communications between parties on the internet and ensure privacy.
This technique relies on properties of modular arithmetic and groups.

\section{History of Public Key Cryptography}

Mathematical records go as far back as the Mesopotamians and the cuneiform digits they imprinted in clay tables.
The Greeks made many contributions to mathematics.
Euclid proved that there are an infinite number of primes which play a central role in cryptography.
The Sieve of Eratosthenes is an algorithm for finding prime numbers that is still taught in beginning number theory classes 3,000 years later \parencite{abstract}.
The Greeks also began early efforts in cryptography with a tool known as the Scytale cipher in which a strip of parchment was wrapped around a thick stick.
The recipient would need a stick of the same thickness to decipher the original message \parencite{history}.
The Romans developed the Caesar cipher (described below), and the Germans developed the Enigma encryption scheme during the first World War.
Both of these ciphers are in a class called substitution ciphers.
Once the Germans were aware that part of their loss in the first World War was due to the fact that their communications had been intercepted by Britain, they embraced the superior techniques provided by the Enigma schemes \parencite{history}.
As fate would have it, Britain again deciphered German communications with the help of Polish cryptanalysts and brilliant mathematicians such as Alan Turing.

One of the problems with early cryptographic methods is that they required sharing some sort of key prior to communication.
As worldwide telecommunications continued to scale, the prospect of sharing secret keys ahead of time became impractical.
Near the dawn of the computer age in 1976, Bailey Whitfield Diffie and Martin Hellman came up with a method to exchange secret keys over an untrusted medium \parencite{diffie}.
The idea is simple to explain.
How do two parties exchange secrets when the communications channel is untrusted?
Alice sends a box and an open lock to Bob.
Bob places his secret, his own box and open lock inside of Alice's box and secures it with her lock.
Alice opens her lock and retrieves Bob's secret.
She places her secret inside Bob's box and secures it with his lock and sends it back.
The exchange is complete at this point.
This exchange is secure as long as the locks used are sufficiently strong.


A short time later in 1977, Rivest, Shamir, and Adleman created the algorithm which bears their initials, the RSA Algorithm.
Clifford Cocks had originally discovered the scheme by extending the work of James H. Ellis.
This was unknown to the authors of the RSA Algorithm because Cocks was working for the British government.
The British Government Communications Headquarters classified the work as secret and it was not released until 1997, long after Rivest, Shamir, and Adleman had published their independent work \parencite{shamir-bio}.

Ronald L. Rivest is a professor at the Massachusetts Institute of Technology and is co-author of the well-known \textit{Introduction to Algorithms}.
He is a member of the CalTech/MIT Voting Technology Project and has served as a committee member advising the Election Assistance Commission.
Dr. Rivest has won many awards during his career, including the 2002 ACM Turing award in conjunction with Shamir and Adleman \parencite{rivest-bio}.

Adi Shamir is an Israeli cryptographer born in Tel Aviv.
He earned his PhD at the Weizmann Institute and studied abroad in England and the United States where he met Rivest and Adleman at MIT.
Dr. Shamir helped establish the company RSA Data Security with the other two authors of the algorithm.
RSA Data Security later spun off other successful companies such as Verisign \parencite{shamir-bio}.

Leonard Adleman earned his Bachelor's degree at Berkeley in California and briefly attended graduate school at San Francisco State College.
He dropped out to be a computer programmer at the Federal Reserve Bank.
He returned to Berkeley and earned his PhD in 1976.
Dr. Adleman took on an assistant professor position at MIT where he met Rivest and Shamir.
He used his programming skills to attempt to break functions that Rivest and Shamir would develop.
It took 42 attempts before the other two authors found a function that Adlemen could not break.
Adleman also performed research on the AIDS virus that led him to discover DNA computing.
He founded the Laboratory for Molecular Science at USC in 1995 \parencite{adleman-bio}.

In 1979, Adi Shamir published “How to Share a Secret” in which he details an encryption scheme based on polynomial interpolation \parencite{shamir}.
He uses an example of needing to sign pay checks.
The mechanism works by requiring $k$ pieces of a key, and having $k-1$ pieces of the key reveal no information about the secret.
Imagine a situation where a medium-sized company wants three of five executives to be able to sign checks.
No two would be able to reconstruct another key, and any three could perform the task in the absence of others.

Mathematicians are fast at work on creating new methods of encryption, especially with the prospect of scalable quantum computing on the horizon.

\section{Number Theory and Modular Arithmetic}

The foundation of the RSA algorithm is built upon number theory.
Prime numbers will play a central role in this cast of characters.
The following theorems are the puzzle pieces that compose the inner mechanism of the algorithm.

\begin{thm}
	\textbf{Fundamental Theorem of Arithmetic}
	Let $n$ be an integer such that $n > 1$.
	Then
	$$n = p_1 p_2 \dots p_k,$$
	where $p_1, \dots, p_k$ are primes (not necessarily distinct).
	Furthermore, this factorization is unique; that is, if
	$$n = q_1 q_2 \dots q_l,$$
	then $k=l$ and the $q_i$'s are just the $p_i$'s rearranged (\cite{abstract}).
\end{thm}
Students learn this in grade school.
31 is prime, 341 is the product of the primes 11 and 31.

\begin{thm}
	\textbf{Division Algorithm}
	For all integers $a$ and $m$ with $m > 0$, there exist unique integers $q$ and $r$ such that

	$$a = mq + r$$

	where $0 \le r < m$ (\cite{long-proofs}).
\end{thm}

The Division Algorithm formalizes the process of dividing $a$ by $q$.
Using $a=43$ and $q=2$, one can rewrite $a/q$ as $43 = 21(2) + 1$.

\begin{thm}
	\textbf{Greatest common divisor}
	Let $a$ and $b$ be integers.
	If $c | a$ and $c | b$, then $c$ is said to be a common divisor of $a$ and $b$.
	The greatest common divisor of $a$ and $b$ is the largest integer $d$ such that $d | a$ and $d | b$.
	This number is denoted $\gcd(a, b)$ (\cite{long-proofs}).
\end{thm}

One of the steps of the RSA algorithm requires finding the modular inverse of two numbers which relies on the greatest common divisor (GCD).
This step will be analyzed in depth later.

\begin{thm}
	\textbf{Modular arithmetic}
	For integers $a$, $r$, and $m$, it is said that $a$ is congruent to $r$ modulo $m$, and one writes $a \equiv r\pmod{m}$, if $m | (a - r)$ (\cite{long-proofs}).
\end{thm}

If the prime numbers are the protagonist of our story, then modular arithmetic is the antagonist.
Pardon the pun, but modular arithmetic is the clockwork behind the algorithm.
It is frequently called "clock arithmetic" because of the ubiquity of a clock metaphor.
Think of a 12-hour wall clock.
The hours pass by each day, but the hour is always in some interval between zero and twelve hours.
This is an incredibly useful phenomenon!
13 hours after 4 is the same as $13 + 4 \pmod{12}$ which is equal to the remainder of 17 divided by 12, or 5.
In other words, if it is 4 now, it will be 5 in 13 hours.

\begin{thm}
\textbf{Properties of modular arithmetic}
Assume that $a, b, c, d,$ and $m$ are integers, $a \equiv b\pmod{m}$ and $c \equiv d\pmod{m}$ and $p$ is prime.
Then,
\begin{itemize}
	\item $a + c \equiv b + d\pmod{m}$
	\item $a - c \equiv b - d\pmod{m}$
	\item $a \cdot c \equiv b \cdot d\pmod{m}$
	\item If $p \nmid a$, then $\gcd(a, b) = 1$.
	\item If $a | bc$ and $\gcd(a, b) = 1$, then $a | c$.
	\item If $p | bc$, then $p | b$ or $p | c$ (or both).
\end{itemize}
(\cite{long-proofs})
\end{thm}

One of the earliest uses of cryptography was by Julius Caesar.
The shift cipher, also known as a Caesar cipher, relies on these properties of modular arithmetic (\cite{discrete}).
It is not a robust cipher by modern standards, but is a useful example.
To begin, choose an offset $k$ that is $0 < k < N$ where N is the size of your alphabet.
Before assuming 26 simple letters, consider capitalization and punctuation!
An offset of 0 will not encrypt the message, and an offset of $k > N$ is equivalent to $k \pmod{N}$.
To convert the message "ET TU, BRUTE", convert each character to a number.
Starting with $"A" = 0$ assign an integer to each successive letter.
Finally, assign 26 to a space and 27 to a comma.
One gets $[4, 19, 26, 19, 20, 27, 26, 1, 17, 20, 19, 4]$.
Choosing $k=7$, compute $(c + 7) \pmod{28}$.
The result is $$[11, 26, 5, 26, 27, 6, 5, 8, 24, 27, 26, 11]$$ which is the string "L F ,GFIY, L".
This encrypted string would be sent to the receiver instead of the original message.
This process is easily reversed by calculating $(c - 7) \pmod{28}$ and converting each number back to the original alphabet.

One may notice that an attacker could simply, and trivially given modern methods, try all possible $k$ shifts of indices.
To mitigate this, a sender and receiver may agree on a random permutation of the alphabet, making it much more difficult to decipher \parencite{history}.
These sorts of substitution ciphers are still vulnerable to frequency attacks, though.

\begin{thm}
\textbf{Euler's totient function}
The Euler $\phi$-function is the map $\phi: \mathbb{N} \rightarrow \mathbb{N}$ defined by $\phi(n) = 1$ for $ = 1$ and, for $n > 1, \phi(n)$ is the number of positive integers $m$ with $1 \le m < n$ and $\gcd(m, n) = 1$ (\cite{abstract}).
\end{thm}

In other words, the totient function represents the number of positive integers that are coprime with $n$.
For a prime number $p$, this is simply $p - 1$ since no number is coprime with itself.
In general, computing $\phi(n)$ requires computing the prime factorization of $n$.

\begin{itemize}
	\item \raggedright $\phi(41) = 40$. Because 41 is prime.
	\item $\phi(12) = 4$. Because \{1, 5, 7, 11\} are coprime with 12.
\end{itemize}

\section{Modular inverse}

Generating the key pairs for the RSA algorithm requires computing the modular inverse of $d$ given
$$de \equiv 1\pmod{\phi(N)}$$
where $e$ and $\phi(N)$ are known values.

\begin{thm}
	\textbf{Bézout's identity}
	If $a$ and $b$ are positive integers, then there exist integers $k$ and $l$ such that
	$$\gcd(a, b) = ak + bl$$ (\cite{long-proofs}).
\end{thm}

The integers $k$ and $l$ can be found efficiently using the Extended Euclidean Algorithm.

\subsection{Extended Euclidean Algorithm}

The following procedure follows the method described by Brian Kell \parencite{cmu-ext}.
The programming language implementation of this algorithm is more difficult to follow due to loops and variable-swapping.

Beginning with $a=93060$ and $b=307$, use the division algorithm to find the values $q$ and $r$.
$a$ and $b$ were chosen such that $\gcd(a, b) = 1$.
Back-substitute $a$ and $b$ appropriately, and iterate until the remainder is zero.

\begin{tabularx}{340pt}{ |c|c|c|c|X| }
\hline
{\bf c} & {\bf d} & {\bf q} & {\bf r} & {\bf out} \\
\hline
93060 & 307 & 303 & 39 & $39 = 93060 - 303\dot307
\newline = a - 303b$ \\
\hline
307 & 39 & 7 & 34 & $34 = 307 - 39\dot7
\newline = b - 7(a - 303b)$ \\
& & & & $= -7a + 2122b$ \\
\hline
39 & 34 & 1 & 5 & $5 = 39 - 34
\newline = (a - 303b) - (-7a + 2122b)$ \\
& & & & $= 8a - 2425b$ \\
\hline
34 & 5 & 6 & 4 & $4 = 34 - 6\dot5
\newline = (-7a + 2122b) - 6(8a-2425b)$ \\
& & & & $= -55a + 161672b$ \\
\hline
5 & 4 & 1 & 1 & $1 = 5 - 4
\newline = (8a - 2425b) - (-55a + 161672b)$ \\
& & & & $= 63a - 19097b$ \\
\hline
\end{tabularx}

\paragraph*{}
Bézout's identity tells us that
$$\begin{aligned}
	\gcd(93060, 307) &= 63\cdot93060 - 19097\cdot307 \\
	1 &= 63\cdot93060 - 19097\cdot307 \\
	93060\cdot63 &\equiv 1 \pmod{19097\cdot307} \\
\end{aligned}$$

Most undergraduate articles and texts will simply tell you to use the Extended Euclidean Algorithm and plug that result into a formula.
$$73963\equiv-19097 \pmod{93060}$$
Sure enough,
$$73963\cdot307\equiv1\pmod{93060}.$$

\subsection{How it works}
To parse this idea and build an intuition, consider a much smaller example.
Given $\gcd(3, 7) = 1$, use Bézout's identity and the Extended Euclidean Algorithm to find
$$-2\cdot3+1\cdot7=1.$$
This relationship can be visualized on a number line.
Below are the first few multiples of 7 along with the nearest multiple of 3 that is \textit{greater than or equal} to a multiple of 7.

\begin{tikzpicture}[thick]
    \draw(0,0)--(12.5,0);
    \foreach \x/\xtext in {0/0,1.5/7,3/14,4.5/21,6/28,7.5/35,9/42,10.5/49,12/56}
      \draw(\x,0pt)--(\x,3pt) node[above] {\xtext};
    \foreach \x/\xtext in {0/0,1.9/9,3.2/15,4.5/21,6.4/30,7.7/36,9/42,10.9/51,12.2/57}
      \draw(\x,0pt)--(\x,-3pt) node[below] {\xtext};
 \end{tikzpicture}

Now consider the sequence of \textit{differences} between multiples of 7 and its nearest neighbor.
This results in the sequence
$$[9-7, 15-14, 21-21, 30-28, 36-35, 42-42, 51-59, 57-56, \dots]$$
or
$$[2, 1, 0, 2, 1, 0, 2, 1, \dots].$$
The pattern should be clear, and one knows it repeats because of the properties of modular arithmetic.

Going back to the original values of $(307, 93060)$, one can perform the same operation.
$$\begin{aligned}
	&0,  39,  78, 117, 156, 195, 234, 273,\\
	&5,  44,  83, 122, 161, 200, 239, 278,\\
	&10, 49,  88, 127, 166 \dots\\
\end{aligned}$$
The cyclic pattern here is a bit more complicated.
If one calculates the 63rd value, the result is 1!
Bézout's identity guarantees this, and the Extended Euclidean Algorithm is the most efficient way of finding the result.

\includegraphics[width=0.8\textwidth]{./figure1.png}

Consider the graph of these discrete differences of multiples as a continuous function.
It resembles a periodic function; one might even call it "cyclic".
To investigate this idea further, mathematicians need groups!
We know how to find the modular inverse, but what exactly is it?

\section{Groups}

\begin{dfn}
	\textbf{Partition}
	A partition $\mathcal{P}$ of a set X is a collection of nonempty sets $X_1, X_2, \dots$ such that $X_i \cap X_j = \emptyset$ for $i \ne j$ and $\cup_k X_k = X$ \parencite{abstract}.
\end{dfn}

The integers modulo 6 form 6 partitions of the set $\mathbb{Z}$.
This is written as $\mathbb{Z}_6$.

$$\begin{aligned}
	\relax[0] &= \{\dots, -12, -6, 0, 6, 12, \dots\} \\
	[1] &= \{\dots, -11, -5, 1, 7, 13, \dots\} \\
		&\vdots \\
	[5] &= \{\dots, -7, -1, 5, 11, \dots\} \\
\end{aligned}$$

One should note that the intersection of any subset with another subset is $\emptyset$.
Also, the union of all of these sets is equivalent to $\mathbb{Z}$, thus satisfying the definition.

\begin{dfn}
	\textbf{Equivalence class}
	Let $\sim$ be an equivalence relation on a set X and let $x \in X$.
	Then $[x] = \{y\in X: y \sim x\}$ is called the equivalence class of $x$ \parencite{abstract}.
\end{dfn}

Referring back to the previous list of partitions, $[x]$ represents an \textit{equivalence class} of $\mathbb{Z}_n$.
For example, the equivalence class $[3]$ in $\mathbb{Z}_6$ contains the integer 3 as well as every multiple $\{3 \pm 6n | n \in \mathbb{Z}, n \ne 0\}$.
These are the integers $\{\dots, -15, -9, -3, 3, 9, 15, \dots\}$.

\subsection{Group operations}

We can perform addition and multiplication in $\mathbb{Z}_6$.

\begin{tabularx}{340pt}{ |X|X| }
	\hline
	$3 + 4 \pmod{6} = 1$ & $3*4 \pmod{6} = 0$ \\
	\hline
	$2 + 13 \pmod{6} = 3$ & $5*2 \pmod{6} = 4$ \\
	\hline
	$3 - 5 \pmod{6} = 4$ & $2*(-2) \pmod{6} = 2$ \\
	\hline
\end{tabularx}

\begin{dfn}
	A \textbf{group} $(G, \circ)$ is a set $G$ together with a law of composition $(a, b) \mapsto a \circ b$ that satisfies the following axioms.
\begin{itemize}
	\item The composition is \textbf{associative}.
		That is, $$(a\circ b) \circ c = a \circ (b \circ c)$$ for $a, b, c \in G$.
	\item There exists an element $e \in G$, called the \textbf{identity element}, such that for any element $a \in G$ $$e \circ a = a \circ e = a.$$
	\item For each element $a \in G$, there exists an \textbf{inverse element} in $G$, denoted by $a^{-1}$, such that $$a \circ a^{-1}=a^{-1}\circ a = e$$ \parencite{abstract}.
\end{itemize}
\end{dfn}

If the group $G$ has the property that $a \circ b = b \circ a$ for all $a, b \in G$, then $G$ is called an \textbf{abelian group} or a \textbf{commutative group}.
The set $\mathbb{Z}$ with the binary operation of addition form an abelian group.
The set $\mathbb{Z}_n$ under the operation of addition forms an abelian group.
The set $\mathbb{Z}_n$ with the operation of \textit{multiplication} fails to form a group at all because a multiplicative inverse of 0 does not exist \parencite{abstract}.

This is a good point to revisit the \textbf{modular inverse}.
Modular multiplication tables can be built similar to the multiplication tables one uses in grade school.
These are called Caley tables.
Here are the multiplication tables for $\mathbb{Z}_7$ and $U(8)$.

\begin{tabular}{c | c c c c c c c}
	 & 0 & 1 & 2 & 3 & 4 & 5 & 6 \\
    \cline{1-8}
	0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
	1 & 0 & 1 & 2 & 3 & 4 & 5 & 6 \\
	2 & 0 & 2 & 4 & 6 & 1 & 3 & 5 \\
	3 & 0 & 3 & 6 & 2 & 5 & 1 & 4 \\
	4 & 0 & 4 & 1 & 5 & 2 & 6 & 3 \\
	5 & 0 & 5 & 3 & 1 & 6 & 4 & 2 \\
	6 & 0 & 6 & 5 & 4 & 3 & 2 & 1 \\
\end{tabular}
\quad
\begin{tabular}{c | c c c c c c}
	 & 1 & 2 & 3 & 4 & 5 & 6 \\
    \cline{1-7}
	1 & 1 & 2 & 3 & 4 & 5 & 6 \\
	2 & 2 & 4 & 6 & 1 & 3 & 5 \\
	3 & 3 & 6 & 2 & 5 & 1 & 4 \\
	4 & 4 & 1 & 5 & 2 & 6 & 3 \\
	5 & 5 & 3 & 1 & 6 & 4 & 2 \\
	6 & 6 & 5 & 4 & 3 & 2 & 1 \\
\end{tabular}
\newline
The integers modulo 7, written as $\mathbb{Z}_7^*$, do not form a group because there is no multiplicative inverse of 0.
We can get around this problem by eliminating the zeros from the table above.
This is called the \textbf{group of units} and is denoted $U(8)$.
It's Caley table is on the right above.
The properties of groups tell us that there is some value
	$$a \circ a^{-1}=a^{-1}\circ a = e$$
where $e$ is the identity.
What is the modular inverse of $3\pmod{7}$?
Looking at the multiplication table above, one can see that
$$3\cdot5\pmod{7} = 5\cot3\pmod{7} = 1$$
The group $U(8)$ is cyclic, so
$$3\cdot(5 \pm 7n)\equiv1\pmod{7}$$
where $n \ne 0$.
This shows that there are an infinite number of solutions.

\section{Multiplying Large Integers}

The encryption and decryption steps of the RSA Algorithm require computing the products of very large integers.
93,673 only requires 17 bits of storage, or less than 3 bytes.
What about a number such as $26^{93673}$?
The Python programming language will gladly attempt to compute it and succeeds on a modern laptop.

\begin{python}
>>> pow(26, 93673).bit_length()
440305
\end{python}

That is a massive number over 134,000 digits long!
Thankfully, this particular result is not needed, but the result modulus another number.
For example, using a number easily computed by hand, what is $26^{307}\pmod{47}$?
Start by converting the exponent to binary.
307 in base-2 is equal to 100110011.
Exponentiation works in $\mathbb{Z}_n$ because of the properties listed earlier.
An exponent is simply repeated multiplication.
If $b \equiv a^x \pmod{n}$ and $c\equiv a^y\pmod{n}$, then $bc \equiv a^{x+y}\pmod{n}$.
$$
\begin{aligned}
	26^{307} &= 26^{2^0+2^1+2^4+2^5+2^8} \\
			 &\equiv 307^{2^0}+307^{2^1}+307^{2^4}+307^{2^5}+307^{2^8}\pmod{47} \\
\end{aligned}
$$
Calculate $2^i$ where $i=0, 1, 4, 5, 8$.
$$26^{2^1}=676\equiv18 \pmod{47}$$
This can be squared to find $26^{2^2}\pmod{47}$.
$$\begin{aligned}
	26^{2^2}&\equiv (26^{2^1})^2\pmod{47} \\
			&\equiv (18)^2\pmod{47} \\
			&\equiv 324\pmod{47} \\
			&\equiv 42\pmod{47} \\
\end{aligned}$$
Use the fact that $(a^{2^n})^2\equiv a^{2\cdot2^n}\equiv a^{2^{n+1}}\pmod{n}$ \parencite{abstract}.
$$\begin{aligned}
	26^{2^4}&\equiv 14\pmod{47} \\
	26^{2^5}&\equiv 8\pmod{47} \\
	26^{2^8}&\equiv 2\pmod{47} \\
	26^{307}&\equiv 26*18*14*8*2 \pmod{47}\\
	&\equiv 104832 \pmod{47}\\
	&\equiv 22 \pmod{47}\\
\end{aligned}$$

Other than speed, this method saves memory as well since the largest number the computer must store is $2\log_2{exp}$ bits.
In general, an $n$-bit number multiplied by an $n$-bit number is $2n$-bits long.
There are two approaches known as left-to-right (LR) and right-to-left (RL) depending on the direction that the bits of the exponent are scanned.
The method above uses the RL method, but LR is more common in hardware implementations of the calculation \parencite{rsa-hw}.

\section{RSA Algorithm}

The RSA Algorithm is a public key cryptography system.
The first step is to generate a public key that is shared and a private key that must never be shared.

\subsection{Key selection}

\begin{enumerate}
	\item The receiver chooses two prime numbers $p$ and $q$ and calculates their product $N$.
	\item Compute $\phi(N)$.  Since $N$ is the product of two primes, $\phi(N) = (p - 1)(q - 1)$.
	\item Choose a number $e$ that is relatively prime to both $N$ and $\phi(N)$.  65,537 is frequently used due to its desirable binary properties (\citeauthorfirstlast{brilliant-rsa}).
	\item $(e, N)$ is the \textbf{public key}.
	\item Calculate the modular inverse $d$ of $e\pmod{\phi(N)}$. $(d, N)$ is the \textbf{private key} (\cite{long-proofs}).
\end{enumerate}

\subsubsection{Key selection - example}

\begin{enumerate}
	\item Choose $p=331$ and $q=283$.  Compute $N = 331\cdot283 = 93673$.
	\item Compute $\phi(N) = (p - 1)(q - 1) = 330\cdot 282 = 93060$.
	\item Choose $e = 307$.
	\item $(307, 93673)$ is the \textbf{public key}.
	\item Calculate the modular inverse $d$ of $$307\pmod{\phi(93673)}=307\pmod{93060}=73963$$
		This step was demonstrated previously using the Extended Euclidean Algorithm.
		$(73963, 93673)$ is the \textbf{private key} where the first value is the modular inverse $d$ and the second is the product $N$ computed in step 1.
\end{enumerate}

\subsection{Message Encryption and Decryption}

Use the same phrase from the Caesar cipher and encrypt "ET TU, BRUTE".
Begin as before by converting the letters to numbers, which again results in $[4, 19, 26, 19, 20, 27, 26, 1, 17, 20, 19, 4]$.
Using the public key $(307, 93673)$, compute $$c^{307} \pmod{93673}$$ for each character.
These numbers can be quite large, so be mindful of the limitations of the software you are using as many programming languages fail to warn of overflow errors.
Python has native support for large integers which makes this calculation easy to perform without precision issues.

\begin{python}
>>> et_tu = [4, 19, 26, 19, 20, 27, 26, 1, 17, 20, 19, 4]
>>> ciph = list(map(lambda n: pow(n, 307, 93673), et_tu))
>>> ciph
[43857, 26421, 21480, 26421, 44114, 11186,
21480, 1, 54440, 44114, 26421, 43857]
>>>
\end{python}

These are the integers that someone would send over an insecure medium to the receiver.
On the other end, the receiver uses the private key $(73963, 93673)$ to compute
$$i^{73963}\pmod{93673}$$
for each integer received.
\begin{python}
>>> msg = list(map(lambda n: pow(n, 73963) % 93673, ciph))
>>> msg
[4, 19, 26, 19, 20, 27, 26, 1, 17, 20, 19, 4]
>>>
\end{python}

A simple review quickly validates that this is indeed our original message!
It is worth highlighting once more that the private key is \textbf{never} shared.
The RSA algorithm is not paricularly fast, especially for handheld devices like cellular phones.
It is typically used to exchange a randomly-generated key that is unique to each HTTPS session between a client and server \parencite{cf-session}.

Another invaluable use of the RSA algorithm is as a Digital Signature mechanism.
The authors realized early on that a secure digital communications system needs to be built on a foundation of trust and verification \parencite{rsa-paper}.
If Bob sends Alice his public key, how can Alice be sure that Eve didn't intercept the conversation and send her own key?

\subsection{Euler’s theorem}

The RSA algorithm works because of Euler's theorem.

\begin{thm}
	\textbf{Euler's Theorem}
	Let $a$ and $n$ be integers such that $n > 0$ and $\gcd(a, n) = 1$.
	Then $a^{\phi(n)}\equiv 1 \pmod{n}$ \parencite{abstract}.
\end{thm}

Recall that step 5 of the key selection requires finding

$$td\equiv 1\pmod{\phi(N)}.$$

Using the definition of modular congruence,

$$td = 1 + k\cdot \phi(N)$$

for some integer $k$.
The message is encrypted by finding $a^t \pmod{N}$ and decrypted by raising the encrypted message to the power of $d\pmod{N}$ \parencite{long-proofs}.

$$(a^t)^d \pmod{N}$$

This gives back $a$, the original message.

$$\begin{aligned}
	(a^t)^d &= a^{td} \\
			&= a^{1+k\cdot\phi(N)} \\
			&= a\cdot \left( a^{\phi(N)} \right)^k \\
			&\equiv a\cdot1^k \\
			&\equiv a\pmod{N} \\
\end{aligned}$$

Euler's theorem may appear a bit magical at first.
Consider the Caley table of integers $\pmod{9}$.

{
\centering
\begin{tblr}{
		colspec = {c | c c c c c c c c},
		cell{2}{2} = {yellow7},
		cell{6}{3} = {yellow7},
		cell{3}{6} = {yellow7},
		cell{8}{5} = {yellow7},
		cell{5}{8} = {yellow7},
		cell{9}{9} = {yellow7},
	}
	 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
    \cline{1-9}
	1 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
	2 & 2 & 4 & 6 & 8 & 1 & 3 & 5 & 7 \\
	3 & 3 & 6 & 0 & 3 & 6 & 0 & 3 & 6 \\
	4 & 4 & 8 & 3 & 7 & 2 & 6 & 1 & 5 \\
	5 & 5 & 1 & 6 & 2 & 7 & 3 & 8 & 4 \\
	6 & 6 & 3 & 0 & 6 & 3 & 0 & 6 & 3 \\
	7 & 7 & 5 & 3 & 1 & 8 & 6 & 4 & 2 \\
	8 & 8 & 7 & 6 & 5 & 4 & 3 & 2 & 1 \\
\end{tblr}\par
}

Take note of the entries with a value of 1.
These only occur when a number is coprime with 9.
The number of 1 entries in the Caley table is the same value as $\phi(n)$.
For example, 11 is coprime with 9.

$$\begin{aligned}
	2 \equiv 11 \pmod{9} \\
	11^2 \equiv 4 \pmod{9} \\
	11^3 \equiv 8 \pmod{9} \\
	11^4 \equiv 7 \pmod{9} \\
	11^5 \equiv 5 \pmod{9} \\
	11^6 \equiv 1 \pmod{9} \\
\end{aligned}$$

This process works the same even if you start with a value of 2.
Increasing the power will always work towards a value of 1 as long as the GCD of $a$ and $n$ is 1.

\section{Code Review}

A full copy of the source code can be found on \href{https://gitlab.com/wificidr/maths/-/blob/main/rsa/src/main.rs}{Gitlab}.
Rust was chosen for a number of reasons.
It is more performant that Python, and it is designed for safety.
The mathematical encrypt and decrypt functions are both maps $f: \mathbb{N} \rightarrow \mathbb{N}$, but programming languages leverage narrower types for efficiency.
Characters are stored as bytes of values $\{b | 0 \le b \le 255\}$.
RSA keys larger than the word size (typically 64-bits) of one's machine must be stored as arrays of bytes, so the \href{https://docs.rs/num-bigint/latest/num_bigint/index.html}{num\_bigint} library is used.

\begin{lstlisting}
fn encrypt(
  message: &Vec<u8>, pub_key: &(BigUint, BigUint)
) -> Vec<BigUint> {
  let mut encrypted = vec![];
  for ch in message.iter() {
    encrypted.push(BigUint::from(*ch).modpow(&pub_key.0, &pub_key.1));
  }
  encrypted
}

fn decrypt(
  cipher: &Vec<BigUint>, priv_key: &(BigUint, BigUint)
) -> Vec<u8> {
  let mut decrypted = vec![];
  for ch in cipher.iter() {
    decrypted.push(ch.modpow(&priv_key.0, &priv_key.1).to_bytes_be()[0]);
  }
  decrypted
}
\end{lstlisting}

\subsection{Challenges and improvements}

The implementation is slow and tests take a little over 3 seconds on an Intel i7 processor using 1024 bit keys.
The \emph{num\_bigint} library is a general-purpose numeric library and is not specialized for cryptographic purposes.
Current standards support a finite set of key lengths, so stack memory could be used instead of heap-allocated arrays.
The mathematics of the implementation are sound, and performance is largely outside the scope of this document.

However, the RSA algorithm is computationally expensive, even with specialized and highly-tuned software.
TLS uses elliptic curves or the RSA algorithm to securely exchange what are known as session keys.
These are exchanged using the method described by Diffie and Hellman \parencite{diffie}.
These session keys use symmetric encryption based on linear algorithms that are much faster to compute \parencite{cf-session}.

\section{Probability of Breaking the Encryption Key}

The safety of the RSA algorithm lies in the ability to quickly factor prime numbers.
According to Judson, it is still an open question whether RSA can be broken.
Companies have offered large cash prizes for the proof of the factorization of several large primes \parencite{abstract}.
It is conjectured that prime factorization is NP-hard \parencite{np}.
This means that a solution takes exponential time to compute, but much can be verified in polynomial time.
The National Institute of Standars and Technology (NIST) currently recommunds an RSA key length of 2048 bits \parencite{nist}.
The primes chosen for $p$ and $q$ in the algorithm need to be 1024 bits each.
Their product is a prime integer with over 600 digits!
Calculating the prime factoriaztion of such a large number is computationally infeasible at the moment.

\section{Vulnerabilities and the Future of RSA}

The advent of quantum computing is the single biggest threat to the security of the RSA Algorithm.
Even though the technology has not been scaled to the point of being able to factor large numbers, the prospect of breaking RSA and elliptic curve cryptography has spured new advances in the cryptography space.
In 1995, Peter Shor proposed an algorithm that can factor integers in polynomial time using quantum computers.
Shor's algorithm works using a similar method to the way that physicists use backscattering to discover the properites of crystal lattices \parencite{ibm}.
In light of these developments, the National Security Agency (NSA) opened a competition in 2016 to challenge industry and academic professionals to find "post-quantum" algorithms \parencite{cloudflare}.
These new algorithms are not set to be standardized by the NIST until 2024, but some organizations such as Cloudflare and Amazon have started deploying the new key agreement algorithm known as Kyber \parencite{pq}.
There is currently no replacement for RSA or elliptic curves as a signing mechanism, and this is an area of active research.

Another drawback of the RSA algorithm is the lack of "forward secrecy".
Servers on the internet tend to use certificates that last months or years that are based on the site's public RSA key.
Since the numbers used to generate the public certificate are part of the public RSA key, a leaked private key could compromise any other recorded coversations that took place with that key.
Internet surveillance is common and widespread, so elliptic curve cryptography has been a popular alternative for a symmetric key exchange algorithm due to its use of ephemeral session keys \parencite{cloudflare-sec}.

Most of the vulnerabilites associated with the RSA algorithm are based on software implementation issues and not the mathematical foundation of the protocol.

\section{Conclusion}

Prime numbers are the building blocks of the integers and the foundation of number theory.
Even modern computers are much less efficient and accurate performing floating point computations than they are with integer calculations.
Society around the world has come to rely on the internet to exchange information, socialize, and perform commerce.
The RSA algorithm has been the maintstay of internet security for many years, and it will be around for several more.
The next generation of algorithms rely on properties of prime numbers and groups as well, so number theory has many more secrets to reveal to us in the years ahead.

$\square$

\clearpage
\printbibliography

\end{document}
