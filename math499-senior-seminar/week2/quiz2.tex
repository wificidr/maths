\documentclass[11pt]{article}
\usepackage[utf8]{inputenc} % Required for inputting international characters
\usepackage[T1]{fontenc} % Output font encoding for international characters
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{pifont}
\usepackage{tikz}

\usepackage{mathpazo} % Palatino font

\usepackage[backend=biber,
style=apa,
citestyle=authoryear]{biblatex}

\addbibresource{../references.bib}

\begin{document}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\begin{titlepage} % Suppresses displaying the page number on the title page and the subsequent page counts as page 1
	\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for horizontal lines, change thickness here

	\center % Centre everything on the page

	%------------------------------------------------
	%	Headings
	%------------------------------------------------

	\textsc{\LARGE American Public University}\\[1.5cm] % Main heading such as the name of your university/college

	%------------------------------------------------
	%	Title
	%------------------------------------------------

	\HRule\\[0.4cm]

	{\huge\bfseries Quiz 2 MATH499}\\[0.4cm] % Title of your document

	\HRule\\[1.5cm]

	%------------------------------------------------
	%	Author(s)
	%------------------------------------------------

	\begin{minipage}{0.4\textwidth}
		\begin{flushleft}
			\large
			\textit{Author}\\
			Daniel \textsc{Justice} % Your name
		\end{flushleft}
	\end{minipage}
	~
	\begin{minipage}{0.4\textwidth}
		\begin{flushright}
			\large
			\textit{Professor}\\
			Dr. Stephen \textsc{Flink} % Supervisor's name
		\end{flushright}
	\end{minipage}

	%------------------------------------------------
	%	Date
	%------------------------------------------------

	\vfill\vfill\vfill
	{\large\today} % Date, change the \today to a set date if you want to be precise
	\vfill

\end{titlepage}

%----------------------------------------------------------------------------------------

\section*{1.}

Explain why the set of vectors $\{v_1, v_2, v_3\}$ is independent.
Provide a short but complete answer to your response.

\begin{align}
	v_1 =
	\begin{bmatrix}
		1 \\
		-3 \\
		8 \\
	\end{bmatrix}, v_2 =
	\begin{bmatrix}
		-3 \\
		8 \\
		5 \\
	\end{bmatrix}, v_3 =
	\begin{bmatrix}
		2 \\
		-2 \\
		6 \\
	\end{bmatrix}
\end{align}

\paragraph{Solution}

The simplest answer is that the vectors are linearly independent.
No pair is a scalar multiple of another.
This can also be seen in that the determinant of the three vectors is non-zero (\cite{linear}).

$$
\begin{vmatrix}
	1 & -3 & 2 \\
	-3 & 8 & -2 \\
	8  & 5 & 6 \\
\end{vmatrix} = -106
$$

$\square$

\section*{2.}

Evaluate the limit as $x$ approaches 3:

$$f(x) = \frac{\frac{1}{x} - \frac{1}{3}}{x-3}$$

\paragraph{Solution}

First, try to evaluate the limit by pluggin in 3.
Unfortunately, this results in the indeterminate form $\frac{0}{0}$.
We can first try factoring then resort to L' Hospital's rule if necessary.

$$\begin{aligned}
	\frac{\frac{1}{x} - \frac{1}{3}}{x-3} \\
	\frac{\frac{3}{3x} - \frac{x}{3x}}{x-3} \\
	\frac{\frac{3 - x}{3x}}{x-3} \\
	\frac{-\frac{x - 3}{3x}}{x-3} \\
	-\frac{x - 3}{3x} \frac{1}{x-3} \\
	\frac{-1}{3x} \\
\end{aligned}$$

We can now evaulate the limit at 3 and see that the result is $\frac{-1}{9}$.

$\square$

\section*{3.}

Find the derivative for the function $f(x) = (x^3 - 7)^2$

\paragraph{Solution}

Using the chain rule:

$$\begin{aligned}
	2(x^3 - 7)^1(3x^2) \\
	6x^2(x^3 - 7) \\
	6x^5 - 42x^2 \\
\end{aligned}$$

$\square$

\section*{4.}

Find the inverse of the matrix, if it exists

\begin{align}
	\begin{bmatrix}
		-1 & -6 \\
		6 & 3 \\
	\end{bmatrix}
\end{align}

\paragraph{Solution}

$-1(3) - -6(6) = -3 + 36 = 33$, so the matrix is invertible (\cite{linear}).

$$\begin{aligned}
	A^{-1} &= \frac{1}{ad - bc}
	\begin{bmatrix}
		d & -b \\
		-c & a \\
	\end{bmatrix} \\
		   &=  \frac{1}{33}
	\begin{bmatrix}
		3 & 6 \\
		-6 & -1 \\
	\end{bmatrix} \\
		   &=
	\begin{bmatrix}
		\frac{3}{33} & \frac{6}{33} \\
		\frac{-6}{33} & \frac{-1}{33} \\
	\end{bmatrix} \\
		   &=
	\begin{bmatrix}
		\frac{1}{11} & \frac{2}{11} \\
		\frac{-2}{11} & \frac{-1}{33} \\
	\end{bmatrix} \\
\end{aligned}$$

$\square$

\section*{5.}

From the following set of data, find the mean, median, mode, range and sample standard deviation.

45, 62, 23, 50, 76, 50, 99, 72, 51, 80

\paragraph{Solution}

Start by sorting the values:

23, 45, 50, 50, 51, 62, 72, 76, 80, 99

Sample standard deviation is given by (\cite{stat}):

$$\sigma = \sqrt{\frac{\sum (X - \overline{X})^2}{n - 1}}$$

\begin{itemize}
	\item mean $= \sum X / len(X) = 608 / 10 = 60.8$
	\item mode $= 50$
	\item range $= 99 - 23 = 76$
	\item sample $\sigma \approx 21.59$
\end{itemize}

$\square$

\section*{6.}

Consider the graph of the function given by $f(x) = (x - 4) / (x^2 - 16)$.
List all asymptotes, points of discontinuity and holes in the graph.

\paragraph{Solution}

There is a point of discontinuity at $x = 4$ because the demoninator evaulates to $0$ at this point.
The limit of this point exists.

$$\lim_{x \rightarrow 4} = \frac{1}{8}$$

There is avertical asymptote at $x = -4$.

$$\lim_{x \rightarrow -4+} = \infty$$

$$\lim_{x \rightarrow -4-} = -\infty$$

$\square$

\section*{7.}

Find the average value from 0 to $\pi$ for the function $f(x) = sin(x)$.

\paragraph{Solution}

The average value of a continuous function is given by (\cite{calculus}):

$$\frac{1}{b-a} \int_a^b f(x) \; dx$$

$$\begin{aligned}
	&= \frac{1}{\pi - 0} \int_0^{\pi} \sin{x} \; dx \\
	&= \frac{1}{\pi} -\cos{x} \big |_0^{\pi} \\
	&= \frac{1}{\pi} \left( -\cos{\pi} - -\cos{0} \right) \\
	&= \frac{2}{\pi} \\
\end{aligned}$$

$\square$

\section*{8.}

Find the integral of $$\int e^x \cos{(x)} \; dx$$

\paragraph{Solution}

Using integration by parts:

$$\begin{aligned}
	\int u \; dv &= uv - \int v \; du \\
	u &= \cos{x}, dv = e^x \\
	du &= -\sin{x}, v = e^x \\
	\int e^x \cos{(x)} \; dx &= e^x \cos{x} + \int e^x \sin{x} \; dx \\
\end{aligned}$$

Repeat integration by parts for the second term and simplify:

$$\begin{aligned}
	u &= -\sin{x}, dv = e^x \\
	du &= -\cos{x}, v = e^x \\
	\int e^x \cos{(x)} \; dx &= e^x \cos{x} + e^x \sin{x} - \int e^x \cos{x} \; dx \\
	2\int e^x \cos{(x)} \; dx &= e^x \cos{x} + e^x \sin{x} \\
	\int e^x \cos{(x)} \; dx &= \frac{e^x \cos{x} + e^x \sin{x}}{2} \\
	\int e^x \cos{(x)} \; dx &= \frac{e^x (\cos{x} + \sin{x})}{2} \\
\end{aligned}$$

$\square$

\section*{9.}

In reviewing a scatterplot for a set of data, no relation is visible among the data points.
Should linear regression be used to further analyze the data? Explain why or why not.

\paragraph{Solution}

Linear regression should \textbf{not} be used to analyze this data.
If the correlation coefficient is near zero, linear regression will be of little use analyzing the data or predicting outcomes.
A statistician may perform a t-test on the sample to help determine the significance of the correlation coefficient (\cite{stat}).
The formula for the correlation coefficient, $r$ is:

$$r = \frac{n(\sum xy) - (\sum x)(\sum y)}{\sqrt{[n(\sum x^2) - (\sum x)^2][n(\sum y^2) - (\sum y)^2]}}$$

where $n$ is the number of data points (\cite{stat}).

$\square$

\section*{10.}

Given that: $x = c_1 \cos{4t} + c_2 \sin{4t}$ is a two-parameter family of solutions of $x'' + 16x = 0$.
Find a solution of the initial-value problem.

$$x'' + 16x = 0, x \left(\frac{\pi}{2}\right) = -2, x'\left(\frac{\pi}{2}\right) = 1$$

\paragraph{Solution}

Find $c_1$.

$$\begin{aligned}
	x \left( \frac{\pi}{2} \right) = -2 &= c_1 \cos(2\pi) + c_2 \sin(2\pi) \\
	-2 &= c_1(1) + 0 \\
	-2 &= c_1 \\
\end{aligned}$$

Find $c_2$.

$$\begin{aligned}
	x &= -2 \cos(4t) + c_2 \sin(4t) \\
	x' &= 8 \sin(4t) - c_2 \sin(4t) \\
	x' \left( \frac{\pi}{2} \right) = 1 &= 8 \sin(2\pi) + c_2 \sin(2\pi) \\
	1 &= 0 + c_2(1) \\
	1 &= 0 + c_2(1) \\
	1 &= c_2 \\
\end{aligned}$$

Plug and play:

$$x = -2 \cos{4t} + \frac{1}{4}\sin{4t}$$

$\square$

\section*{11.}

Determine whether the point (4, 9) is in the feasible set of this system of inequalities

$$\begin{aligned}
	5x + 4y \le 63 \\
	x + y \le 12 \\
	6x + 9y \ge 97 \\
	x \ge 0 \\
	y \ge 0 \\
\end{aligned}$$

\paragraph{Solution}

Evaluate each expression.
If all are true, then the point is in the feasible region.

$$\begin{aligned}
	5(4) + 4(9) \le 63\implies T  \\
	4 + 9 \le 12\implies F  \\
	6(4) + 9(9) \ge 97 \implies T \\
	4 \ge 0 \implies T \\
	9 \ge 0 \implies T \\
\end{aligned}$$

The second expression is false, so the point $(4, 9)$ lies \textbf{outside} the feasible region.

$\square$

\section*{12.}

Which of the following two combinations is larger: ${}^{100}C_{46}$ or ${}^{99}C_{45}$?

\paragraph{Solution}

We can compare these using the binomial coefficient and then cross multiplying.

$$\binom{n}{k} = \frac{n!}{k!(n-k)!}$$

$$\begin{aligned}
	\binom{100}{46} &= \frac{100!}{46!54!} \\
	\binom{99}{45} &= \frac{99!}{45!54!} \\
	\frac{100!}{46!54!} &\propto \frac{99!}{45!54!} \\
	100!45!54! &\propto 99!46!54! \\
	100 &\propto 46 \\
	100 > 46 &\implies \binom{100}{46} > \binom{99}{45} \\
\end{aligned}$$

$\square$

\section*{13.}

Suppose a packing box contains 24 smartphones, of which two phones are defective.
If, while setting up a store display, a salesclerk selects 10 smartphones at random from the box without replacement, what is the probability that both defective smartphones will be selected and put on display?

\paragraph{Solution}

There are $\binom{22}{8}$ ways to choose a working phone, and $\binom{2}{2}$ ways to choose two defective phones.
The population is $\binom{24}{10}$, so the probability $P$ is:

$$P_{\text{choosing two defective phones}} = \frac{\binom{22}{8} \binom{2}{2}}{\binom{24}{10}} \approx 0.163$$

$\square$

\section*{14.}

An engineer who is studying the tensile strength of a steel alloy intended for use in golf club shafts knows that tensile strength is normally distributed with $\sigma = 60$ psi.
A random sample of 12 specimens has a mean tensile strength of $\overline{x} = 3450$ psi.

Test the hypothesis that the mean tensile strength of this steel alloy is 3500 psi against the alternative that the mean tensile strength is not 3500 psi.
Conduct your test at the $\alpha= .01$ level of significance.
Clearly indicate the null hypothesis, the alternative hypothesis, the level of significance, the critical region, the test statistic value, and your conclusion in the space below.

\paragraph{Solution}

Null hypothesis: $\overline{x} = 3500$ psi

Alternative hypothesis: $\overline{x} \ne 3500$ psi

Level of significance: $\alpha = 0.01$

Critical region: $z \pm 2.58$ (\cite{stat})


Using the z-test:

$$z = \frac{\overline{X}-\mu}{\sigma / \sqrt{n}}$$

$$z = \frac{3450 - 3500}{60 / \sqrt{12}} \approx -2.89$$

Since -2.89 falls in the critical region, the engineer should \textbf{reject the null hypothesis}.

$\square$

\section*{15.}

Given the parametric equations $x = 3t^2- 4; y = 6t + 4$.
Find $\frac{dy}{dx}$ in terms of t.

\paragraph{Solution}

$$\begin{aligned}
	\frac{dx}{dt} &= 6t \\
	\frac{dy}{dt} &= 6 \\
	\frac{dy}{dx} &= \frac{\frac{dy}{dt}}{\frac{dx}{dt}} \\
	\frac{dy}{dx} &= \frac{6}{6t} = \frac{1}{t} \\
\end{aligned}$$

$\square$

\clearpage
\printbibliography

\end{document}
