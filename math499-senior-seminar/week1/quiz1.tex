\documentclass[11pt]{article}
\usepackage[utf8]{inputenc} % Required for inputting international characters
\usepackage[T1]{fontenc} % Output font encoding for international characters
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{pifont}
\usepackage{tikz}

\usepackage{mathpazo} % Palatino font

\usepackage[backend=biber,
style=apa,
citestyle=authoryear]{biblatex}

\addbibresource{../references.bib}

\begin{document}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\begin{titlepage} % Suppresses displaying the page number on the title page and the subsequent page counts as page 1
	\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for horizontal lines, change thickness here

	\center % Centre everything on the page

	%------------------------------------------------
	%	Headings
	%------------------------------------------------

	\textsc{\LARGE American Public University}\\[1.5cm] % Main heading such as the name of your university/college

	%------------------------------------------------
	%	Title
	%------------------------------------------------

	\HRule\\[0.4cm]

	{\huge\bfseries Quiz 1 MATH499}\\[0.4cm] % Title of your document

	\HRule\\[1.5cm]

	%------------------------------------------------
	%	Author(s)
	%------------------------------------------------

	\begin{minipage}{0.4\textwidth}
		\begin{flushleft}
			\large
			\textit{Author}\\
			Daniel \textsc{Justice} % Your name
		\end{flushleft}
	\end{minipage}
	~
	\begin{minipage}{0.4\textwidth}
		\begin{flushright}
			\large
			\textit{Professor}\\
			Dr. Stephen \textsc{Flink} % Supervisor's name
		\end{flushright}
	\end{minipage}

	%------------------------------------------------
	%	Date
	%------------------------------------------------

	\vfill\vfill\vfill
	{\large\today} % Date, change the \today to a set date if you want to be precise
	\vfill

\end{titlepage}

%----------------------------------------------------------------------------------------

\section*{1.}

An aircraft can fly 1,330 miles with the wind in 5 hours and travel the same distance against the wind in 7 hours.
What is the speed of the wind in miles per hour?

\paragraph{Solution}

Let $v_a$ represent the velocity of the aircraft and $v_w$ represent the velocity of the wind.
We have two equations with two unknowns.

$$
\begin{aligned}
	(v_a + v_w)5 &= 1330 \\
	(v_a - v_w)7 &= 1330 \\
\end{aligned}
$$

Solve for one unknown.

$$
\begin{aligned}
	7v_a &= 1330 + 7v_w \\
	v_a &= 190 + v_w \\
\end{aligned}
$$

Back substitute into the other equation.

$$
\begin{aligned}
	((190 + v_w) + v_w)5 &= 1330 \\
	(190)5 + 10 v_w &= 1330 \\
	v_w &= \frac{1330 - (190)5}{10} \\
	v_w &= 38
\end{aligned}
$$

The velocity of the wind is 38 miles per hour.

$\square$

\section*{2.}

Solve the equation for $x$: $\sqrt{x+6} + \sqrt{2-x}=4$.

\paragraph{Solution}

$$
\begin{aligned}
	(\sqrt{x+6} + \sqrt{2-x})^2 &= 4^2 \\
	2(\sqrt{x+6}\sqrt{2-x}) + x + 6 + 2 - x &= 16 \\
	\sqrt{x+6}\sqrt{2-x} - 4 &= 0 \\
	(x + 6)(-x + 2) - 16 &= 0 \\
	-x^2 - 4x + 12 - 16 &= 0 \\
	x^2 + 4x + 4 &= 0 \\
	(x + 2)^2 &= 0 \\
	x &= -2 \\
\end{aligned}
$$

$\square$

\section*{3.}

Express the following equation in standard form, identify the conic section, and state the locations of its center or vertex.

$$x^2-6x+y^2+8y=5$$

\paragraph{Solution}

Begin by completing the square (\cite{calculus}).

$$
\begin{aligned}
	(x - 3)^2 + 9 + (y + 4)^2 + 16 &= 15 + 16 + 9 \\
	(x - 3)^2 + (y + 4)^2 &= 30 \\
\end{aligned}
$$

This is the equation of a circle with center at (3, -4) and radius $\sqrt{30}$.

$\square$

\section*{4.}

Use DeMorgan's Laws to simplify the expression.

$$S' \cap (S \cup T)'$$

\paragraph{Solution}

De Morgan's law: $\overline{A \cup B} = \overline{A} \cap \overline{B}$ (\cite{discrete}).

$$
\begin{aligned}
	S' \cap (S \cup T)' &= S' \cap S' \cap T' \\
	&= S' \cap T' \\
	&= (S \cup T)' \\
\end{aligned}
$$

$\square$

\section*{5.}

Show that $\cos{\theta} = \sin{(\theta + 30^\circ)} + \cos{(\theta + 60^\circ)}$.

\paragraph{Solution}

Using the trigonometry identities (\cite{trig}):

\begin{itemize}
	\item $\sin(u \pm v) = \sin{u}\cos{v} \pm \cos{u}\sin{v}$
	\item $\cos(u \pm v) = \cos{u}\cos{v} \pm \sin{u}\sin{v}$
\end{itemize}

$$
\begin{aligned}
	\cos{\theta} &= \sin{\theta}\cos{30} + \cos{\theta}\sin{30} + \cos{\theta}\cos{60} - \sin{\theta}\sin{60} \\
				 &= \sin{\theta}\frac{\sqrt{3}}{2} + \cos{\theta}\frac{1}{2} + \cos{\theta}\frac{1}{2} - \sin{\theta}\frac{\sqrt{3}}{2} \\
				 &= \frac{1}{2}\cos{\theta} + \frac{1}{2}\cos{\theta} \\
				 &= \cos{\theta}
\end{aligned}
$$

$\square$

\section*{6.}

A developer wants to enclose a rectangular grassy lot that borders a city street for parking.
If the developer has 288 feet of fencing and does not fence the side along the street, what is the largest area that can be enclosed?

\paragraph{Solution}

This is an optimization problem.
The primary equation is $A = hw$ where $h$ is the length of one side adjoining the road and $w$ represents the side parallel to the road.
The secondary equation is $P = 288 = 2h + w$ where $P$ represents perimeter and $A$ represents area.

$$
\begin{aligned}
	w &= 288 - 2h \\
	A &= h(288 - 2h) \\
	  &= 288h - 2h^2 \\
	A' &= 288 - 4h \\
\end{aligned}
$$

The critical point is $A' = 0$ at $h = 72$.
$w = 144$ and the largest area that can be enclosed is \textbf{10,368 square feet}.

$\square$

\section*{7.}

Two wireless telephone transmission centers are 35 km apart.
A cell phone user is 19 km from one of the centers.
At the cell phone user, the angle formed by the two centers is 101 degrees.
Find the distance between the cell phone user and the other transmission center.

\begin{center}
\includegraphics[width=0.5\linewidth]{./fig1.png}
\end{center}

\paragraph{Solution}

This can be solved using the Law of Cosines (\cite{trig}).

$$
\begin{aligned}
	a^2 &= b^2+c^2 - 2bc\cos{A} \\
	35^2 &= 19^2 + c^2 - 2(19)(c)\cos{101} \\
	c^2 - 2(19)(c)\cos{101} - 864 &= 0 \\
	c &= \frac{38\cos{101} \pm \sqrt{(38\cos{101})^2 - 4}}{2} \\
	c &\approx 26\text{km}
\end{aligned}
$$

$\square$

\section*{8.}

If $f(x) = 8x^3 + 3x^2 - x + C$ and $f(2) = 1$, what is the value of $C$?

\paragraph{Solution}

Substitute and solve:

$$
\begin{aligned}
	8(2^3) + 3(2^2) - 2 + C &= 1 \\
	64 + 12 - 2 + C &= 1 \\
	C &= -73 \\
\end{aligned}
$$

$\square$

\section*{9.}

The function $P(x) = 0.65x^2 - 0.048x + 1$ models the approximate population $P$, in thousands, for a species of fish in a local pond, $x$ years after 1997.
During what year did the fish population reach 42,216 fish?

\paragraph{Solution}

This can be solved using the quadratic formula.

$$x = \frac{-b \pm \sqrt{b^2-4ac}}{2a}$$

... or using a CAS.

$$
\begin{aligned}
	np.roots([0.65, -0.048, -42215]) \\
	array([ 254.88227856, -254.8084324 ]) \\
	1997 + 254.88227856 \approx 2251.88227855641
\end{aligned}
$$

Using the positive answer since the arrow of time moves to the right, we find that the population reaches 42,216 near the year \textbf{2252}.

$\square$

\section*{10.}

Write a short essay to describe the steps involved in solving a system of inequalities where there are two unknowns and two linear inequalities.
Demonstrate your procedure by working a problem.

\paragraph{Solution}

The steps for solving a system of linear inequalities is very similar to the process used in problem 1 of this quiz.
To find the intersection of two linear equations, solve for one variable and back-substitute it into the other equation to solve for the second unknown.
There a couple additional consideration when working with inequalities, though.
It is important to remember to flip the "direction" of the inequality when multiplying or dividing by a negative number.
Degenerate solutions are possible where there is no solution, infinite solutions, or a singular solution.
When a solution does exist, it could be a two-dimensional region or a point (the intersection of more than two linear equations).
An example will best demonstrate the process.

Given the system below, find the region of the solution:

$$
\begin{aligned}
	4x - 3y > 6 \\
	7x + 9y \le 10 \\
	y \ge -2 \\
\end{aligned}
$$

There are three equations, so we need to find three solutions representing the intersections of each pair of equations.
The simplest place to start is to substitute $y$ from the third equation into the first two.

$$
\begin{aligned}
	4x - 3(-2) > 6 \\
	4x + 6 > 6 \\
	4x > 0 \\
	x > 0 \\
\end{aligned}
$$

likewise:

$$
\begin{aligned}
	7x + 9(-2) \le 10 \\
	7x - 18 \le 10 \\
	7x \le 28 \\
	x \le 4 \\
\end{aligned}
$$

This gives us our first two solutions:

\begin{itemize}
	\item $x > 0, y \ge -2$
	\item $x \le 4, y \ge -2$
\end{itemize}

Next, we need to solve for the intersection of the first two equations.

$$
\begin{aligned}
	7x + 9y \le 10 \\
	7x \le 10 - 9y \\
	x \le \frac{10}{7} - \frac{9y}{7} \\
	x \le \frac{10 - 9y}{7} \\
\end{aligned}
$$

Note the change of inequality when dividing by -57.

$$
\begin{aligned}
	4x - 3y > 6 \\
	4 \left (\frac{10 - 9y}{7} \right ) - 3y > 6 \\
	\frac{40 - 36y}{7} - \frac{21y}{7} > 6 \\
	40 - 57y > 42 \\
	-57y > 2 \\
	y < -\frac{2}{57} \\
\end{aligned}
$$

Finally:

$$
\begin{aligned}
	4x - 3 \left (-\frac{2}{57} \right) > 6 \\
	4x + \frac{6}{57} > 6 \\
	4x > 6 - \frac{6}{57} \\
	x > \frac{3}{2} - \frac{1}{38} \\
	x > \frac{28}{19} \\
\end{aligned}
$$

$$
\begin{aligned}
	7x + 9 \left(-\frac{2}{57} \right ) \le 10 \\
	7x -\frac{18}{57} \le 10 \\
	7x \le 10 + \frac{18}{57} \\
	x \le \frac{10}{7} + \frac{6}{133} \\
	x \le \frac{28}{19} \\
\end{aligned}
$$

The last set of solutions is:

\begin{itemize}
	\item $x > \frac{28}{19}, y < -\frac{2}{57}$
	\item $x \le \frac{28}{19}, y < -\frac{2}{57}$
\end{itemize}

This results in the following shaded region:

\begin{center}
\includegraphics[width=0.5\linewidth]{./figure2.png}
\end{center}

$\square$

\clearpage
\printbibliography

\end{document}
