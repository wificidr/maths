use num_bigint::{BigInt, BigUint, Sign};
use num_integer::Integer;

fn main() {
    let p = BigUint::from(331u32);
    let q = BigUint::from(283u32);
    let t = BigUint::from(307u32);
    let phi = (p.clone() - BigUint::from(1u8)) * (q.clone() - BigUint::from(1u8));
    let d = BigUint::try_from(modinv(&t, &phi).expect("failed to find modular inverse"))
        .expect("int to uint conversion failed");
    let n = p.clone() * q.clone();
    println!(
        "p: {}, q: {}, N: {}, phi: {}, t: {}, d: {}",
        p, q, n, phi, t, d
    );

    let pub_key: (BigUint, BigUint) = (t, n.clone());
    let priv_key: (BigUint, BigUint) = (d, n);
    let message = String::from("Hello, world!");
    println!("{:?}", message.as_bytes());
    let encrypted = encrypt(&message.as_bytes().to_vec(), &pub_key);
    println!("{:?}", encrypted);
    let decrypted = decrypt(&encrypted, &priv_key);
    println!("{:?}", decrypted);
    println!("{}", String::from_utf8_lossy(decrypted.as_slice()));
}

fn encrypt(message: &Vec<u8>, pub_key: &(BigUint, BigUint)) -> Vec<BigUint> {
    let mut encrypted = vec![];
    for ch in message.iter() {
        encrypted.push(BigUint::from(*ch).modpow(&pub_key.0, &pub_key.1));
    }
    encrypted
}

fn decrypt(cipher: &Vec<BigUint>, priv_key: &(BigUint, BigUint)) -> Vec<u8> {
    let mut decrypted = vec![];
    for ch in cipher.iter() {
        decrypted.push(ch.modpow(&priv_key.0, &priv_key.1).to_bytes_be()[0]);
    }
    decrypted
}

fn egcd(a: &BigInt, b: &BigInt) -> (BigInt, BigInt, BigInt) {
    let mut a = a.clone();
    let mut b = b.clone();
    let mut x: BigInt = BigInt::from(0u8);
    let mut y: BigInt = BigInt::from(1u8);
    let mut u: BigInt = BigInt::from(1u8);
    let mut v: BigInt = BigInt::from(0u8);
    while a != BigInt::from(0u8) {
        let (q, r) = b.div_rem(&a);
        let (m, n) = (
            x.clone() - u.clone() * q.clone(),
            y.clone() - v.clone() * q.clone(),
        );
        let (bp, ap, xp, yp, up, vp) = (
            a.clone(),
            r.clone(),
            u.clone(),
            v.clone(),
            m.clone(),
            n.clone(),
        );
        a = ap;
        b = bp;
        x = xp;
        y = yp;
        u = up;
        v = vp;
    }
    (b, x, y)
}

fn modinv(a: &BigUint, m: &BigUint) -> Option<BigInt> {
    let (gcd, x, _) = egcd(
        &BigInt::from_biguint(Sign::Plus, a.clone()),
        &BigInt::from_biguint(Sign::Plus, m.clone()),
    );
    if gcd != BigInt::from(1u8) {
        None
    } else {
        let r = x.modpow(
            &BigInt::from(1u32),
            &BigInt::from_biguint(Sign::Plus, m.clone()),
        );
        Some(r)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::time::Instant;

    #[test]
    fn test_enc_dec() {
        // openssl prime -generate -bits nbits
        let test_string = String::from("Testing, testing, testing, 1, 2, uh, 12...");
        let little_p = BigUint::from(331u32);
        let little_q = BigUint::from(283u32);
        let little_t = BigUint::from(307u32);
        // 128 bits
        let mid_p = BigUint::from(316172697061287232353574601990182751087u128);
        let mid_q = BigUint::from(291499414154282414228405233684982988787u128);
        let mid_t = BigUint::from(65537u32);
        // 1024 bits
        let big_p = BigUint::parse_bytes(b"153918730228999602421190145221296336821496416347300167176036864279244314463222704434105257980042208314831345344103238453990625141070419416237779255078079343398769316520654199991987050919033382039833629143452404291461037614008145864559682994880589213464097668548126518961469408756114382329820275794981568449549", 10).unwrap();
        let big_q = BigUint::parse_bytes(b"167073132136203102079668675507830384490980816428105593487566856769694960941563535041763373818022569586547693604401902470583030213039495586358130304092467645116107477678100013031368807081071152838217979224480441116037976644155006862597926419747140305612661731245826188355887703826467762662343247996467588449781", 10).unwrap();
        let big_t = BigUint::from(65537u32);
        let cases = vec![
            (little_p, little_q, little_t),
            (mid_p, mid_q, mid_t),
            (big_p, big_q, big_t),
        ];
        for (p, q, t) in cases {
            let phi = (p.clone() - BigUint::from(1u8)) * (q.clone() - BigUint::from(1u8));
            let d = BigUint::try_from(modinv(&t, &phi).expect("failed to find modular inverse"))
                .expect("int to uint conversion failed");
            let n = p.clone() * q.clone();

            let pub_key: (BigUint, BigUint) = (t, n.clone());
            let priv_key: (BigUint, BigUint) = (d, n);

            let start = Instant::now();
            let decrypted = decrypt(
                &encrypt(&test_string.as_bytes().to_vec(), &pub_key),
                &priv_key,
            );
            let duration = start.elapsed();
            println!("Time elapsed in enc/dec is: {:?}", duration);
            assert_eq!(test_string, String::from_utf8_lossy(decrypted.as_slice()));
        }
    }
}
