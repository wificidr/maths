\documentclass[11pt]{article}
\usepackage[utf8]{inputenc} % Required for inputting international characters
\usepackage[T1]{fontenc} % Output font encoding for international characters
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{pifont}
\usepackage{tikz}
\usepackage{float}
\usepackage{hyperref}

\usepackage{mathpazo} % Palatino font

\begin{document}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\begin{titlepage} % Suppresses displaying the page number on the title page and the subsequent page counts as page 1
	\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for horizontal lines, change thickness here

	\center % Centre everything on the page

	%------------------------------------------------
	%	Headings
	%------------------------------------------------

	\textsc{\LARGE American Public University}\\[1.5cm] % Main heading such as the name of your university/college

	%------------------------------------------------
	%	Title
	%------------------------------------------------

	\HRule\\[0.4cm]

	{\huge\bfseries Test 1 MATH460}\\[0.4cm] % Title of your document

	\HRule\\[1.5cm]

	%------------------------------------------------
	%	Author(s)
	%------------------------------------------------

	\begin{minipage}{0.4\textwidth}
		\begin{flushleft}
			\large
			\textit{Author}\\
			Daniel \textsc{Justice} % Your name
		\end{flushleft}
	\end{minipage}
	~
	\begin{minipage}{0.4\textwidth}
		\begin{flushright}
			\large
			\textit{Professor}\\
			Dr. Andrew \textsc{Lane} % Supervisor's name
		\end{flushright}
	\end{minipage}

	%------------------------------------------------
	%	Date
	%------------------------------------------------

	\vfill\vfill\vfill
	{\large\today} % Date, change the \today to a set date if you want to be precise
	\vfill

\end{titlepage}

%----------------------------------------------------------------------------------------

\section*{1)}

Find the Fourier series of $f(x), -\pi < x < \pi$ whose graph is a triangle with vertices at $(-\pi, 0), (\pi, 0), (0, \pi)$ and is assumed to have a period of $2\pi$.
Sketch or graph the partial sums up to that including $\cos 5x$ and $\sin 5x$.

\paragraph{Solution}

$f(x)$ is an even function

$$
\begin{aligned}
	f(x) &= a_0 + \sum_{n=1}^{\infty} a_n \cos \frac{n\pi}{L}x \\
	a_0 &= \frac{1}{2\pi} \int_{-\pi}^{0} (x+\pi) dx + \int_{0}^{\pi} (-x+\pi) dx \\
	a_0 &= \frac{1}{\pi} \left[ \frac{x^2}{2} + \pi x \right]_{-\pi}^0 \\
	a_0 &= \frac{\pi}{2} \\
	a_n &= \frac{1}{\pi} \int_{-\pi}^{0} (x + \pi) \cos nx\; dx +
		\frac{1}{\pi} \int_{0}^{\pi} (-x + \pi) \cos nx\; dx \\
\end{aligned}
$$
$$
\begin{cases}
	u = x + \pi & v = \frac{\sin nx}{n} \\
		& \int v = \frac{-\cos nx}{n^2} \\
	du = 1 & dv = \cos nx \\
\end{cases}
$$

$$
\begin{aligned}
	a_n &= \frac{2}{\pi} \left( \frac{1}{n^2} - \frac{\cos \pi n}{n^2} \right) \\
	f(x) &= \frac{\pi}{2} + \frac{2}{\pi} \left( (1-\cos \pi) \cos x + (1-1) \cos 2x + \frac{\cos 3 x}{3^2} \dots \right) \\
		 &= \frac{\pi}{2} + \frac{4}{\pi} \left( \cos x + \frac{\cos 3 x}{9} + \frac{\cos 5 x}{25} \dots \right) \\
\end{aligned}
$$

$\square$

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\linewidth]{./figure1.png}
  \caption{Partial sum plot $f(x), n=7$}
  \label{fig:fig1}
\end{figure}

\section*{2)}

Using Parseval's identity, show that $1+\frac{1}{3^2}+\frac{1}{5^2}+\dots=\frac{\pi^2}{8}$.

\paragraph{Solution}

Parseval's identity:
$$
2a_0^2 + \sum_{n=1}^{\infty}\left(a_n^2 + b_n^2 \right)= \frac{1}{\pi} \int_{-\pi}^{\pi} f(x)^2\;dx
$$

$$
\begin{aligned}
	f(x) &= \begin{cases}
		-1 \text{ if } -\pi < x < 0 \\
		1 \text{ if } 0 < x < \pi \\
	\end{cases} \\
\end{aligned}
$$
$$
\begin{aligned}
	a_n &= 0 \forall{n} \\
	b_n &= 0 \text{ when n is even} \\
	b_n &= \frac{4}{\pi n} \text{when n is odd} \\
\end{aligned}
$$
$$
\begin{aligned}
	\sum_{n=1}^{\infty} b_n^2 &= \frac{1}{\pi} \int_{-\pi}^{\pi} \pm 1 ^2\;dx \\
	\frac{4^2}{\pi^2 n^2}&= \frac{1}{\pi} \left[ 2\pi \right] \\
	\frac{16}{\pi^2 n^2}&= 2 \\
	\frac{16}{n^2}&= 2\pi^2 \\
	\frac{1}{n^2}&= \frac{2\pi^2}{16} \\
	\frac{1}{n^2}&= \frac{\pi^2}{8} \text{ for odd } n\\
\end{aligned}
$$
$$
1+\frac{1}{3^2}+\frac{1}{5^2}+\dots=\frac{\pi^2}{8}
$$

$\square$

\section*{3)}

Find the Fourier transform of $f(x)=\begin{cases}
	-1 & -1 < x < 0 \\
	1 & 0 < x < 1 \\
	0 & \text{otherwise} \\
\end{cases}$

\paragraph{Solution}

$$
\begin{aligned}
	\hat{f}(w) &= \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} f(x) e^{-iwx}\;dx \\
	&= \frac{1}{\sqrt{2\pi}} \left[ \int_{-1}^{0} -1 e^{-iwx}\;dx + \int_{0}^{1} 1 e^{-iwx}\;dx \right]\\
	&= \frac{1}{\sqrt{2\pi}} \left[ \int_{-1}^{0} -e^{-iwx}\;dx + \int_{0}^{1} e^{-iwx}\;dx \right]\\
	&= \frac{1}{\sqrt{2\pi}} \left[ \frac{-ie^{-iwx}}{w}\bigg|_{-1}^{0} +
		\frac{ie^{-iwx}}{w}\bigg|_{0}^{1} \right]\\
	&= \frac{1}{\sqrt{2\pi}} \left[
	\frac{i(e^{iw} - 1)}{w} + \frac{\sin w + i(\cos w - 1)}{w} \right] \\
\end{aligned}
$$

$\square$

\section*{4)}

Show that if $f(x)$ has a Fourier transform, so does $f(x-a)$, and its Fourier transform is $e^{-iwa}\mathfrak{F} \left \{f(x) \right \}$.

\paragraph{Solution}

$$
\begin{aligned}
	e^{-iwa}\mathfrak{F} \left \{f(x) \right \}
	&= \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} f(x-a) e^{-iwx}\;dx \\
	&= \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} f(y) e^{-iw(y+a)}\;dy \\
	&= e^{iwa} \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} f(y) e^{-iwy}\;dy \\
	&= e^{iwa} \mathfrak{F} \left \{f(x) \right \} = e^{-iwa}\mathfrak{F} \left \{f(x) \right \}\\
\end{aligned}
$$

$\square$

\end{document}
