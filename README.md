[![pdm-managed](https://img.shields.io/badge/pdm-managed-blueviolet)](https://pdm.fming.dev)

# Maths

## Supporting packages

- `jupyter-lab`
- `sympy`
- `scipy`
- `nbconvert[webpdf]`
- `matplotlib`
- `networkx`

### Nicety

- [solarized dark](https://github.com/AllanChain/jupyterlab-theme-solarized-dark)

## MATH305 Real Analysis

[real](math305-real-analysis)

## MATH470 Numerical Analysis

[numeric](math470-numerical-analysis)

## MATH412 Graph Theory

[graph](math412-graph-theory)

## MATH418 Topology

[topology](math418-topology)

## MATH460 Applied Maths

[applied](math460-applied-math)

## MATH499 Senior Seminar

[seminar](math499-senior-seminar)
