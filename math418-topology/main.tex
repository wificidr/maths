\documentclass{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{tabularx}
\usepackage[backend=biber,
style=apa,
citestyle=authoryear]{biblatex}

\addbibresource{references.bib}

\begin{document}

\section*{Definitions}

\subsection*{Open sets}

Let (X, $\tau$) be any topological space.  Then the members of $\tau$ are said to be open sets.

\medskip\noindent
If (X, $\tau$) is any topological space, Then

(i) X and $\emptyset$ are open sets,

(ii) the union of any (finite or infinite) number of open sets is an open set, and

(iii) the intersection of any finite number of open sets is an open set.

\medskip\noindent
\textbf{Example.} Let X = \{a, b, c, d, e, f\} and

$\tau_1$ = \{X, $\emptyset$, \{a\}, \{c, d\}, \{a, c, d\}, \{b, c, d, e, f\}\}.

\subsection*{Closed sets}

Let (X, $\tau$) be any topological space.  A subset S of X is said to be a closed set in (X, $\tau$) if its complement in X, namely X$\setminus$S, is open in (X, $\tau$).

\medskip\noindent
If (X, $\tau$) is any topological space, Then

(i) X and $\emptyset$ are closed sets,

(ii) the intersection of any (finite or infinite) number of closed sets is a closed set, and

(iii) the union of any finite number of closed sets is a closed set.

\medskip\noindent
\textbf{Example.} Let X = \{a, b, c, d, e, f\} and

\medskip\noindent
The closed sets are $\emptyset$, X, \{b, c, d, e, f\}, \{a, b, e, f\}, \{b, e, f\}, and \{a\}.

\medskip\noindent
"A subset C of a topological space X is \textbf{closed} provided that its complement $X\setminus C$ is an open set." (\cite{croom})

\begin{enumerate}
	\item the set \{a\} is both open and closed
	\item the set \{b, c\} is neither open nor closed
	\item the set \{c, d\} is open but not closed
	\item the set \{a, b, e, f\} is closed but not open
\end{enumerate}

\subsection*{Hausdorff space}

A topological space (X, $\tau$) is said to be Hausdorff if given any pair of distinct points a, b in X there exist open sets U and V such that a $\in$ U, b $\in$ V, and U $\cap$ V = $\emptyset$.

\subsection*{Boundary point}
"A point x in X is a \textbf{boundary point} of A if x belongs to both $\overline{A}$ and $\overline{(X\setminus{A})}$.
The set of boundary points of A is called the \textbf{boundary} of A and is denoted bdy A." (\cite{croom})

\subsection*{Closure}

"The \textbf{closure} $\overline{A}$ of A is the union of A with its set of limit points: $\overline{A}=A \cup A'$ where A' is the derived set of A." (\cite{croom})

\subsection*{Interior point}
"Let A be a subset of a topological space X.  A point x in A is an \textbf{interior point} of A if there is an open set O containing x and contained in A.
Equivalently, A is called a \textbf{neighborhood} of x.  The \textbf{interior} of A, denoted int A, is the set of all interior points of A." (\cite{croom})
\\

\subsection*{Accumulation point}
"Let (X, $\tau$) be a topological space and A a subset of X.  A point x in X is a \textbf{limit point, cluster point,} or \textbf{accumulation point} of A if every open set containing x contains a point of A distinct from X.
The set of limit points of A is called the \textbf{derived set} of A." (\cite{croom})

\subsection*{Homeomorphic}

"Topological spaces X and Y are \textbf{topologically equivalent} or \textbf{homeomorphic} if there is a one-to-one function f: X$\rightarrow$Y from X onto Y for which both f and the inverse function $f^{-1}$ are continuous.
The function f is called a \textbf{homemorphism}." (\cite{croom})

\subsection*{Continuous}

"Let (X, $\tau$) and (Y, $\tau$') be topological spaces, f: X→Y a function, and a a point of X.  Then f is \textbf{continuous} at a provided that for each open set V in Y containing f(a) there is an open set U in X containing a such that f(U) $\subset$ V.
The function f is \textbf{continuous} if it is continuous at each point of its domain." (\cite{croom})

\subsection*{Convergence}

Let X be a topological space and $\left \{x_n\right \}_{n=1}^{\infty}$ a sequence of points of X.
Then $\left \{x_n\right \}_{n=1}^{\infty}$ \textbf{converges} to the point x $\in$ X, or x is a \textbf{limit} of the sequence, if for each open set in O containing x there is a positive integer N such that $x_n \in O$ for all $n \ge N$.

\section*{Separation Axioms}

\medskip\noindent
\textbf{Example.} Let X = \{a, b, c, d, e, f\} and

\medskip\noindent
$\tau_1$ = \{X, $\emptyset$, \{a\}, \{c, d\}, \{a, c, d\}, \{b, c, d, e, f\}\}.

\medskip\noindent
The closed sets are $\emptyset$, X, \{b, c, d, e, f\}, \{a, b, e, f\}, \{b, e, f\}, and \{a\}.

\subsection*{$T_0$ space}
A space is a $T_0$ space if for each pair $a$, $b$ of distinct points of X there is an open set containing one of the points but not the other.

\subsection*{$T_1$ space}
A space is a $T_1$ space if for each pair $a$, $b$ of distinct points of X there are open sets U and V in X such that $a$ belongs to U but $b$ does not, and $b$ belongs to V but $a$ does not.

\subsection*{$T_2$ space}
A space is a $T_2$ space or a \textbf{Hausdorff space} if for each pair $a$, $b$ of distinct points of X there are disjoint open sets U and V in X such that $a$ belongs to U and $b$ belongs to V.

\subsection*{$T_3$ space}
A $T_3$ space or \textbf{regular space} is a $T_1$ space X such that for each closed subset C of X and each point $a$ not in C, there exists disjoint open sets U and V in X such that $a\in U$ and $C\subset V$.

\subsection*{$T_4$ space}
A $T_1$ space X  is a $T_4$ space or \textbf{normal space} provided that for each pair A, B of disjoint closed sets in X there exists disjoint open sets U and V such that A is contained in U and B is contained in V.


\section*{Theorems}

\subsection*{Theorem 4.3}

For any subsets A, B of a topological space X:

\begin{enumerate}
	\item The interior of A is the union of all open sets contained in A and is therefore the largest open set contained in A.
	\item A is open if and only if A = int A.
	\item If A $\subset$ B, then int A $\subset$ int B.
	\item int (A $\cap$ B) = int A $\cap$ int B.
\end{enumerate}

\end{document}
